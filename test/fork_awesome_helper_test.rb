require 'minitest/autorun'
require 'fork_awesome_helper'

include ForkAwesome::Helper

describe ForkAwesome::Helper do
  before do
    ForkAwesome.config.raise_exception = false
  end

  describe "rendering" do
    it "renders the svg" do
      assert_match /<svg.*fa-home.*>.*<\/svg>/, icon("home")
    end

    it "renders the default" do
      svg = icon('xxxxxx')
      path = ForkAwesome::Icon.new(ForkAwesome.config.default).path
      assert_includes svg, path
    end

    it "adds html attributes to output" do
      assert_match /foo="bar"/, icon("home", foo: "bar")
    end

    it "adds label to output" do
      assert_match /Love/, icon("heart", "Love")
      assert_match /Love/, icon("heart") {"Love"}
    end

    it "escapes label once" do
      assert_match /&lt;span&gt;Love&lt;\/span&gt;/, icon("heart", "<span>Love</span>")
      assert_match /Love &mdash;/, icon("heart", "Love &mdash;")
      assert_match /\<span\>Love\<\/span\>/, icon("heart", "<span>Love</span>".html_safe)
    end
  end
end
