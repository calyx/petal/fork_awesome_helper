require 'minitest/autorun'
require 'fork_awesome_helper'

def fa_icon(*args)
  ForkAwesome::Icon.new(*args)
end

describe ForkAwesome::Icon do
  before do
    ForkAwesome.config.raise_exception = true
  end

  describe "when creating" do
    it "raises exception on invalid name" do
      assert_raises(StandardError) do
        fa_icon('xxxxxx')
      end
    end

    it "succeeds on valid name" do
      assert fa_icon("home")
    end

    it "succeeds on valid symbol" do
      assert fa_icon(:home)
    end
  end

  describe "when given custom options" do
    it "includes classes" do
      assert_includes fa_icon(:home).to_svg(class: 'custom'), 'class="fa fa-home custom"'
    end
    it "includes fill" do
      assert_includes fa_icon(:home).to_svg(fill: 'red'), 'fill="red"'
    end
    it "includes custom attributes" do
      assert_includes fa_icon(:home).to_svg(disabled: "true"), 'disabled="true"'
    end
  end

  describe "icon sizing" do
    it "uses defaults" do
      svg = fa_icon(:home).to_svg
      assert_includes svg, 'height="%s"' % ForkAwesome.config.height
      assert_includes svg, 'width="%s"' % ForkAwesome.config.width
    end

    it "accepts either height or width" do
      svg = fa_icon(:home).to_svg(width: 100)
      assert_includes svg, 'height="%s"' % 100
      assert_includes svg, 'width="%s"' % 100
      svg = fa_icon(:home).to_svg(height: "200")
      assert_includes svg, 'height="%s"' % 200
      assert_includes svg, 'width="%s"' % 200
    end
  end

  describe "a11y" do
    it "includes attributes for symbol keys" do
      svg = fa_icon(:home).to_svg("aria-label": "Home")
      assert_includes svg, 'role="img"'
      assert_includes svg, 'aria-label="Home"'
      refute_includes svg, 'aria-hidden'
    end

    it "includes attributes for string keys" do
      svg = fa_icon(:home).to_svg("aria-label" => "Home")
      assert_includes svg, 'role="img"'
      assert_includes svg, 'aria-label="Home"'
      refute_includes svg, 'aria-hidden'
    end

    it "has aria-hidden when no label is passed in" do
      svg = fa_icon(:home).to_svg
      assert_includes svg, 'aria-hidden="true"'
    end
  end

end
