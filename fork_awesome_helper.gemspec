Gem::Specification.new do |spec|
  spec.name          = "fork_awesome_helper"
  spec.version       = "0.1.0"
  spec.authors       = ["elijah"]

  spec.summary       = "Rails helper for ForkAwesome icons"
  spec.homepage      = "https://0xacab.org/calyx/petal/forkawesome_helper"

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir["{lib,icons}/**/*", "LICENSE", "Rakefile", "README.md"]
  end

  spec.add_dependency "activesupport"
  spec.add_development_dependency "minitest"
end
