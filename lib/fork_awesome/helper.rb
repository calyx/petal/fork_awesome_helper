module ForkAwesome
  module Helper
    @@icon_cache = {}

    def icon(*args, &block)
      symbol, label, options = parse_icon_arguments(*args, &block)
      icon_object = (@@icon_cache[symbol] ||= ForkAwesome::Icon.new(symbol))
      svg = icon_object.to_svg(options)
      if label
        label = ERB::Util.html_escape_once(label) unless label.html_safe?
        (svg + " " + label).html_safe
      else
        svg.html_safe
      end
    end

    private

    def parse_icon_arguments(*args, &block)
      symbol  = args.first.to_s
      label   = nil
      options = nil
      if args[1].is_a? String
        label   = args[1]
        options = args[2]
      else
        options = args[1]
      end
      if block_given?
        label = yield
      end
      return [symbol, label, options]
    end
  end
end
